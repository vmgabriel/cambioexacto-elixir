# Dar un cambio Exacto Algortimo Voraz con Elixir

## Hecho por:
- Gabriel Vargas Monroy

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org)

- Se puede ingresar cualquier valor
- Uso de recursividad
- Algortmo Voraz

## Ejecucion

1. Se Carga iex
2. Se ejecuta
```elixir
Ejecucion.main()
```
3. Se obtienen resultados similares a:

![Ejecucion Algoritmo Cambio Exacto](img/cambioMonedas_algoritmo.png "Ejecucion Algoritmo Cambio Exacto")
