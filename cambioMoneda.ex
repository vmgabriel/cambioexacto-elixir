defmodule CambioMoneda do
  def get_cambio(0, denominacion), do: denominacion
  def get_cambio(cambio, denominacion) do
    [valor, _] = moneda_usar = val_monedas(cambio, denominacion)
    ind = Enum.find_index(denominacion, fn([x,_]) -> x == valor end)
    denominacion = List.replace_at(denominacion, ind, moneda_usar)
    get_cambio(val_cambio(cambio, denominacion), denominacion)
  end

  def val_monedas(0, x), do: x
  def val_monedas(_, []), do: []
  def val_monedas(x, [[val, retorno] | _]) when x >= val, do: [val, retorno + 1]
  def val_monedas(x, [_ | tail]), do: val_monedas(x, tail)

  def val_cambio(0, _), do: 0
  def val_cambio(x, []), do: x
  def val_cambio(x, [[val, _] | _]) when x >= val, do: x - val
  def val_cambio(x, [_ | tail]), do: val_cambio(x, tail)
end

defmodule Ejecucion do
  def imprimirCambio([]), do: IO.puts("")
  def imprimirCambio([[x, y] | tail]) do
    IO.puts("La cantidad de monedas de #{x} es: #{y}")
    imprimirCambio(tail)
  end
  # Recursivamente imprime

  def main do
    IEx.configure(inspect: [charlists: :as_lists])

    IO.puts("Dar cambio con la menor cantidad de monedas")
    IO.puts("----")
    denominacion = [ [20, 0], [15, 0], [10, 0], [5, 0], [1, 0] ]
    {cambio, _} = Integer.parse(IO.gets("Ingrese el valor a generar cambio: "))
    denominacion = CambioMoneda.get_cambio(cambio, denominacion)
    IO.puts("---- Cambio a entregar ----")
    imprimirCambio(denominacion)
    :ok
  end
end
